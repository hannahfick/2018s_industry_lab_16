package ictgradschool.industry.designpatternsii.ex02.model;


import javax.swing.table.AbstractTableModel;

public class CourseAdapter extends AbstractTableModel implements CourseListener {

    private Course _adaptee;

    public CourseAdapter(Course courseModel) {

        _adaptee = courseModel;
        _adaptee.addCourseListener(this);

    }

    @Override
    public void courseHasChanged(Course course) {
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return _adaptee.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    final int STUDENT_ID = 0;
    final int STUDENT_LNAME = 1;
    final int STUDENT_FNAME = 2;
    final int STUDENT_EXAM = 3;
    final int STUDENT_TEST = 4;
    final int STUDENT_ASSIGNMENT = 5;
    final int STUDENT_OVERALL = 6;


    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        StudentResult results = _adaptee.getResultAt(rowIndex);


        switch (columnIndex) {

            case STUDENT_ID:
                return results._studentID;
            case STUDENT_LNAME:
                return results._studentSurname;
            case STUDENT_FNAME:
                return results._studentSurname;
            case STUDENT_EXAM:
                return results.getAssessmentElement(StudentResult.AssessmentElement.Exam);
            case STUDENT_TEST:
                return results.getAssessmentElement(StudentResult.AssessmentElement.Test);
            case STUDENT_ASSIGNMENT:
                return results.getAssessmentElement(StudentResult.AssessmentElement.Assignment);
            case STUDENT_OVERALL:
                return results.getAssessmentElement(StudentResult.AssessmentElement.Overall);

            default:
                return "FAIL";
        }


    }

    @Override
    public String getColumnName(int column) {


        switch (column) {
            case STUDENT_ID:
                return "Student ID";
            case STUDENT_LNAME:
                return "Surname";
            case STUDENT_FNAME:
                return "Forename";
            case STUDENT_EXAM:
                return "Exam";
            case STUDENT_TEST:
                return "Test";
            case STUDENT_ASSIGNMENT:
                return "Assignment";
            case STUDENT_OVERALL:
                return "Overall";

            default:
                return super.getColumnName(column);
        }

/**********************************************************************
 * YOUR CODE HERE
 */
    }
}