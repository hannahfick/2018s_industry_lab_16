package ictgradschool.industry.designpatternsii.ex02.gui;

import ictgradschool.industry.designpatternsii.ex02.model.Course;
import ictgradschool.industry.designpatternsii.ex02.model.CourseListener;

public class StatisticsPanelAdapter implements CourseListener {

    StatisticsPanel panel;
    Course course;

      public StatisticsPanelAdapter (StatisticsPanel panel) {
        this.panel = panel;
    }


    @Override
    public void courseHasChanged(Course course) {

        panel.repaint();

    }

    public void setModel (Course course){

        this.course = course;
        course.addCourseListener(this);

    }

    /**********************************************************************
     * YOUR CODE HERE
     */

}
